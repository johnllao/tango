package main

import (
	"net/http"

	"github.com/gomodule/redigo/redis"
	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
)

func httpCmdRun(cmd *cobra.Command, args []string) {

	var router = mux.NewRouter()
	router.HandleFunc("/api/companies/{symbol}", handleRequest(allCompanies))

	var server = http.Server{
		Addr:    ":8080",
		Handler: router,
	}

	var err error
	if err = server.ListenAndServe(); err != nil {
		panic(err)
	}
}

func handleRequest(fn func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) /*http.HandlerFunc*/ {
	return func(w http.ResponseWriter, r *http.Request) {
		//log.Printf("[%s] %s", r.Method, r.RequestURI)
		fn(w, r)
	}
}

func allCompanies(w http.ResponseWriter, r *http.Request) {

	var params = mux.Vars(r)

	var err error
	var redisConn redis.Conn

	redisConn, err = redis.Dial("tcp", ":6379")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer redisConn.Close()

	var company map[string]string
	company, err = redis.StringMap(redisConn.Do("HGETALL", "company/"+params["symbol"]))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("name:     " + company["name"] + "\n"))
	w.Write([]byte("industry: " + company["industry"] + "\n"))
	w.Write([]byte("sector:   " + company["sector"] + "\n"))
}
