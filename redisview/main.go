package main

import (
	"encoding/csv"
	"io"
	"log"
	"os"

	"github.com/gomodule/redigo/redis"
	"github.com/spf13/cobra"
)

var (
	rootCmd = &cobra.Command{
		Use:   "redisview",
		Short: "view redis data",
		Run:   rootCmdRun,
	}

	importCmd = &cobra.Command{
		Use:   "import",
		Short: "import redis data",
		Run:   importCmdRun,
	}

	httpCmd = &cobra.Command{
		Use:   "http",
		Short: "starts http service",
		Run:   httpCmdRun,
	}
)

func init() {

	importCmd.Flags().String("file", "", "path and file name of the company import file")
	rootCmd.AddCommand(importCmd, httpCmd)
}

func main() {

	var err error
	if err = rootCmd.Execute(); err != nil {
		panic(err)
	}
}

func rootCmdRun(cmd *cobra.Command, args []string) {
	cmd.Usage()
}

func importCmdRun(cmd *cobra.Command, args []string) {
	var err error

	var filename = cmd.Flag("file").Value.String()

	var csvFile *os.File
	csvFile, err = os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer csvFile.Close()

	var redisConn redis.Conn
	redisConn, err = redis.Dial("tcp", ":6379")
	if err != nil {
		panic(err)
	}
	defer redisConn.Close()

	_, err = redisConn.Do("FLUSHDB")
	if err != nil {
		panic(err)
	}

	var isHeader = true
	var csvReader = csv.NewReader(csvFile)
	var record []string
	for {
		record, err = csvReader.Read()
		if err == io.EOF {
			break
		}
		if isHeader {
			isHeader = false
			continue
		}
		saveToRedis(redisConn, record)
	}
}

func saveToRedis(conn redis.Conn, record []string) {

	var symbol = record[0]
	var name = record[1]
	var lastSale = record[2]
	var marketCap = record[3]
	var ipoYr = record[4]
	var sector = record[5]
	var industry = record[6]

	var err error
	_, err = conn.Do("HMSET", "company/"+symbol,
		"symbol", symbol,
		"name", name,
		"lastSale", lastSale,
		"marketCap", marketCap,
		"ipoYr", ipoYr,
		"sector", sector,
		"industry", industry,
	)

	if err != nil {
		log.Printf("ERR [%s] %s", symbol, err.Error())
	}
}
