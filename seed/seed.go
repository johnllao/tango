package seed

import (
	"sync"
)

// Seed represents the default seeder
var Seed = NewSeeder(0)

// NewSeeder creates new instance of seeder
func NewSeeder(start int) func() int {
	var l sync.Mutex
	var i = start

	return func() int {
		l.Lock()
		i++
		l.Unlock()
		return i
	}
}
