package seed_test

import (
	"sync"
	"testing"

	"github.com/johnllao/tango/seed"
)

func TestSeeder(t *testing.T) {

	var s1 = seed.Seed()
	if s1 != 1 {
		t.Errorf("Invalid seed value %d", s1)
	}

	var s2 = seed.Seed()
	if s2 != 2 {
		t.Errorf("Invalid seed value %d", s2)
	}

	var s3 = seed.Seed()
	if s3 != 3 {
		t.Errorf("Invalid seed value %d", s3)
	}
}

func TestSeederSafe(t *testing.T) {

	var increment = seed.NewSeeder(0)

	var w sync.WaitGroup
	w.Add(4)

	go func() {
		increment()
		w.Done()
	}()

	go func() {
		increment()
		w.Done()
	}()

	go func() {
		increment()
		w.Done()
	}()

	go func() {
		increment()
		w.Done()
	}()

	w.Wait()

	var s = increment()
	if s != 5 {
		t.Errorf("Invalid seed value %d", s)
	}
}
