package main

import (
	"flag"
	"fmt"
	"net/http"
	"sync"
	"sync/atomic"
	"time"

	"github.com/VividCortex/gohistogram"
)

type empty struct{}

var (
	url  = flag.String("u", "", "url to benchmark")
	conc = flag.Int("c", 1, "number of concurrent clients")
	no   = flag.Int("n", 1, "number of request")

	concChan chan empty
	done     chan empty

	lock          sync.Mutex
	noOfFailedReq int64

	hist *gohistogram.NumericHistogram
)

func init() {
	flag.Parse()

	concChan = make(chan empty, *conc)
	done = make(chan empty)

	hist = gohistogram.NewHistogram(40)
}

func main() {

	for i := 0; i < *no; i++ {
		concChan <- empty{}
		go startReq(i)
	}

	<-done

	fmt.Printf("failed        : %d \n", noOfFailedReq)
	fmt.Printf("mean          : %f \n", hist.Mean())
	fmt.Printf("percentile 100: %d \n", int(hist.Quantile(1)))
	fmt.Printf("percentile  99: %d \n", int(hist.Quantile(0.99)))
	fmt.Printf("percentile  95: %d \n", int(hist.Quantile(0.95)))
	fmt.Printf("percentile  90: %d \n", int(hist.Quantile(0.90)))
	fmt.Printf("percentile  80: %d \n", int(hist.Quantile(0.80)))
	fmt.Printf("percentile  70: %d \n", int(hist.Quantile(0.70)))
	fmt.Printf("percentile  60: %d \n", int(hist.Quantile(0.60)))
	fmt.Printf("percentile  50: %d \n", int(hist.Quantile(0.50)))
}

func startReq(i int) {

	var err error
	var req *http.Request

	var start = time.Now()

	req, err = http.NewRequest("GET", *url, nil)
	if err != nil {
		atomic.AddInt64(&noOfFailedReq, 1)
		return
	}
	req.Header.Set("pragma", "no-cache")

	var resp *http.Response

	var cli http.Client
	resp, err = cli.Do(req)
	if err != nil {
		atomic.AddInt64(&noOfFailedReq, 1)
		return
	}

	if resp.StatusCode != http.StatusOK {
		atomic.AddInt64(&noOfFailedReq, 1)
		return
	}

	var elapsed = time.Since(start)

	<-concChan

	lock.Lock()
	hist.Add(float64(int64(elapsed / time.Millisecond)))
	lock.Unlock()

	if i == *no-1 {
		done <- empty{}
	}
}
