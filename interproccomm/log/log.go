package log

import (
	"flag"
	"log"
	"os"
)

var (
	applog *log.Logger
)

func init() {

	var err error

	var logpath string
	var flags = flag.NewFlagSet("logflags", flag.ContinueOnError)
	flags.StringVar(&logpath, "logpath", "stderr", "path and file name of the log file")
	flags.Parse(os.Args)

	var logwriter *os.File
	if logpath == "stderr" {
		logwriter = os.Stderr
	} else {
		logwriter, err = os.Create(logpath)
		if err != nil {
			logwriter = os.Stderr
		}
	}

	applog = log.New(logwriter, "", log.LstdFlags)
}

// Print writes message to log
func Print(msg string, v ...interface{}) {
	applog.Printf(msg, v...)
}
