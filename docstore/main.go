package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"

	"github.com/boltdb/bolt"
	"github.com/johnllao/tango/utils/encryption"
	"github.com/spf13/cobra"
)

const (
	passPhrase = "5GJXD4zEUAtho2Ia7SssceKLah4w5kjh"
	dbFilename = "./docstore.db"
	bucketName = "store"
)

var (
	rootCmd = &cobra.Command{
		Use:   "docstore",
		Short: "key-value data storage",
		Run:   rootCmdRun,
	}

	setCmd = &cobra.Command{
		Use:   "set",
		Short: "save data into the key-value store",
		Run:   setCmdRun,
	}

	getCmd = &cobra.Command{
		Use:   "get",
		Short: "get value from key-value store",
		Run:   getCmdRun,
	}

	delCmd = &cobra.Command{
		Use:   "del",
		Short: "remove value from key-value store",
		Run:   delCmdRun,
	}

	keysCmd = &cobra.Command{
		Use:   "keys",
		Short: "list all keys",
		Run:   keysCmdRun,
	}

	key   string
	value string
	file  string

	db *bolt.DB
)

func init() {
	setCmd.Flags().StringVar(&key, "k", "", "unique identifier for the value")
	setCmd.Flags().StringVar(&value, "v", "", "value to be stored")
	setCmd.Flags().StringVar(&file, "f", "", "path and filename")

	getCmd.Flags().StringVar(&key, "k", "", "unique identifier for the value")

	delCmd.Flags().StringVar(&key, "k", "", "unique identifier for the value")

	rootCmd.AddCommand(setCmd, getCmd, delCmd, keysCmd)
}

func main() {

	var err error

	db, err = bolt.Open(dbFilename, 0777, nil)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	var interruptChan = make(chan os.Signal)
	signal.Notify(interruptChan)
	go func() {
		<-interruptChan
		db.Close()
	}()

	if err = rootCmd.Execute(); err != nil {
		panic(err)
	}
}

func rootCmdRun(cmd *cobra.Command, args []string) {
	cmd.Help()
}

func setCmdRun(cmd *cobra.Command, args []string) {

	var err error

	var encVal []byte
	if len(value) > 0 {
		encVal = encryption.Encrypt([]byte(value), passPhrase)
	} else {
		var data []byte
		data, err = ioutil.ReadFile(file)
		if err != nil {
			panic(err)
		}
		encVal = encryption.Encrypt(data, passPhrase)
	}

	err = db.Update(func(tx *bolt.Tx) error {
		var txErr error
		_, txErr = tx.CreateBucketIfNotExists([]byte(bucketName))
		return txErr

	})
	if err != nil {
		panic(err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		var txErr error
		var bucket = tx.Bucket([]byte(bucketName))
		txErr = bucket.Put([]byte(key), encVal)
		return txErr
	})
	if err != nil {
		panic(err)
	}
}

func getCmdRun(cmd *cobra.Command, args []string) {

	db.View(func(tx *bolt.Tx) error {
		var bucket = tx.Bucket([]byte(bucketName))
		var encVal = bucket.Get([]byte(key))
		if encVal != nil {
			var decVal = encryption.Decrypt(encVal, passPhrase)
			os.Stdout.Write(decVal)
		} else {
			fmt.Printf("key '%s' not found", key)
		}
		return nil
	})
}

func delCmdRun(cmd *cobra.Command, args []string) {

	var err error

	err = db.Update(func(tx *bolt.Tx) error {
		var bucket = tx.Bucket([]byte(bucketName))
		return bucket.Delete([]byte(key))
	})
	if err != nil {
		panic(err)
	}

}

func keysCmdRun(cmd *cobra.Command, args []string) {

	var err error

	err = db.View(func(tx *bolt.Tx) error {
		var bucket = tx.Bucket([]byte(bucketName))
		var c = bucket.Cursor()
		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			fmt.Println(string(k))
		}
		return nil
	})

	if err != nil {
		panic(err)
	}
}
