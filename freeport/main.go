package main

import (
	"fmt"
	"net"
)

const (
	// NETWORK default network type (e.g. tcp)
	NETWORK = "tcp"
)

func main() {

	var err error
	var listener net.Listener
	listener, err = net.Listen(NETWORK, ":0")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer listener.Close()

	var addr = listener.Addr().(*net.TCPAddr)
	fmt.Printf("Free port: %d \n", addr.Port)
}
