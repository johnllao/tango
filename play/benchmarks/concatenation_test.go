package benchmarks_test

import (
	"testing"

	"github.com/johnllao/tango/play/benchmarks"
)

func BenchmarkConcatFmt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		benchmarks.ConcatSymbolAndPriceFmt("GOOGLE", 1000)
	}
}

func BenchmarkConcatPlus(b *testing.B) {
	for i := 0; i < b.N; i++ {
		benchmarks.ConcatSymbolAndPricePlus("GOOGLE", 1000)
	}
}

func BenchmarkConcatJoin(b *testing.B) {
	for i := 0; i < b.N; i++ {
		benchmarks.ConcatSymbolAndPriceJoin("GOOGLE", 1000)
	}
}

func BenchmarkConcatBytes(b *testing.B) {
	for i := 0; i < b.N; i++ {
		benchmarks.ConcatSymbolAndPriceBytes("GOOGLE", 1000)
	}
}
