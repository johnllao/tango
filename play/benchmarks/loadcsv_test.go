package benchmarks_test

import (
	"encoding/csv"
	"io"
	"os"
	"testing"

	"github.com/johnllao/tango/play/benchmarks"
)

func BenchmarkSyncCSVLoad(b *testing.B) {

	var err error
	var csvFile *os.File

	csvFile, err = os.Open("./companylist.csv")
	if err != nil {
		b.Fatal(err)
	}
	defer csvFile.Close()

	var csvReader = csv.NewReader(csvFile)
	for {
		var record []string
		record, err = csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			continue
		}

		var company = benchmarks.CreateCompanyFromStringArray(record)

		var jsonBytes []byte
		jsonBytes, err = benchmarks.DecodeToJSON(company)
		if err != nil {
			continue
		}
		_ = jsonBytes
	}
}
