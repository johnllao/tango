package benchmarks

import (
	"bytes"
	"encoding/json"
)

// Company company structures
type Company struct {
	Symbol    string  `json:"symbol"`
	Name      string  `json:"name"`
	LastSale  float64 `json:"lastSale"`
	MarketCap string  `json:"marketCap"`
	IPOYear   string  `json:"ipoYear"`
	Industry  string  `json:"industry"`
	Sector    string  `json:"sector"`
}

// CreateCompanyFromStringArray creates instance of Company
func CreateCompanyFromStringArray(record []string) Company {
	return Company{}
}

// DecodeToJSON decode company struct to JSON
func DecodeToJSON(company Company) ([]byte, error) {
	var err error
	var buff *bytes.Buffer
	var encoder = json.NewEncoder(buff)
	encoder.SetIndent("", "  ")
	err = encoder.Encode(company)
	if err != nil {
		return nil, err
	}
	return buff.Bytes(), nil
}
