package benchmarks

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

// ConcatSymbolAndPriceFmt concatenates string and int using fmt
func ConcatSymbolAndPriceFmt(sym string, price int) string {
	return fmt.Sprintf("%s : %d", sym, price)
}

// ConcatSymbolAndPricePlus concatenates string and int using fmt
func ConcatSymbolAndPricePlus(sym string, price int) string {
	return sym + " : " + strconv.Itoa(price)
}

// ConcatSymbolAndPriceJoin concatenates string and int using fmt
func ConcatSymbolAndPriceJoin(sym string, price int) string {
	return strings.Join([]string{ sym, " : ", strconv.Itoa(price) }, "")
}

// ConcatSymbolAndPriceBytes concatenates string and int using fmt
func ConcatSymbolAndPriceBytes(sym string, price int) string {
	var buf bytes.Buffer
	buf.WriteString(sym)
	buf.WriteString(" : ")
	buf.WriteString(strconv.Itoa(price))
	return buf.String()
}