package main

import (
	"fmt"
	"os"
)

func main() {
	var err error
	var buff = make([]byte, 8)
	if _, err = os.Stdin.Read(buff); err != nil {
		fmt.Printf("ERR %s \n", err.Error())
		return
	}
	fmt.Println(string(buff))
	fmt.Println("bye!")
}
