package json

import (
	"encoding/json"
	"strings"
	"testing"
)

type request struct {
	TransactionID string `json:"transactionID"`
	System        string `json:"system"`
}

func TestJSONEncode(t *testing.T) {

	var req = request{
		TransactionID: "MX1924747",
		System:        "MUREX",
	}

	jsonb, err := json.MarshalIndent(req, "", "  ")
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(string(jsonb))
}

func TestJSONDecode(t *testing.T) {

	var data = `{
		"transactionID": "TX12345",
		"system": "MX"
	}`

	var reader = strings.NewReader(data)
	var decoder = json.NewDecoder(reader)
	decoder.DisallowUnknownFields()

	var req request
	var err = decoder.Decode(&req)
	if err != nil {
		t.Log(err)
	}

	t.Logf("done - %s", t.Name())
}

func TestJSONDecodeMispelledFieledName(t *testing.T) {

	var data = `{
		"transactionID": "TX12345",
		"systems": "MX"
	}`
	var reader = strings.NewReader(data)
	var decoder = json.NewDecoder(reader)
	decoder.DisallowUnknownFields()

	var req request
	var err = decoder.Decode(&req)
	if err != nil {
		t.Log(err)
	}

	t.Logf("done - %s", t.Name())
}
func BenchmarkJSONEncode(b *testing.B) {

	var req = request{
		TransactionID: "MX1924747",
		System:        "MUREX",
	}

	b.ResetTimer()

	for n := 0; n < b.N; n++ {
		_, err := json.MarshalIndent(req, "", "  ")
		if err != nil {
			b.Error(err)
			return
		}
	}
}
