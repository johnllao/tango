package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
)

func main() {

	var rootDir = "./"

	var err error
	var rootDirInfos []os.FileInfo
	rootDirInfos, err = ioutil.ReadDir(rootDir)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	var totalFilesSize int64
	var dirInfo os.FileInfo
	for _, dirInfo = range rootDirInfos {
		if dirInfo.IsDir() {
			var dirSize = calcDirSize(dirInfo.Name())
			fmt.Printf("%s : %s \n", dirInfo.Name(), formatSize(dirSize))
		} else {
			totalFilesSize += dirInfo.Size()
		}
	}
	fmt.Printf("%s : %s", "all files", formatSize(totalFilesSize))
}

func calcDirSize(path string) int64 {
	var total int64

	filepath.Walk(path, func(path string, info os.FileInfo, walkErr error) error {
		if info != nil && !info.IsDir() {
			total += info.Size()
		}
		return nil
	})
	return total
}

func formatSize(value int64) string {
	if value > 1000000000 {
		return strconv.FormatFloat(float64(value)/1000000000, 'f', 6, 64) + " GB"
	}
	if value > 1000000 {
		return strconv.FormatFloat(float64(value)/1000000, 'f', 6, 64) + " MB"
	}
	if value > 1000 {
		return strconv.FormatFloat(float64(value)/1000, 'f', 6, 64) + " KB"
	}
	return strconv.FormatFloat(float64(value), 'f', 6, 64) + " bytes"
}
