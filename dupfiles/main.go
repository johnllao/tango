package main

import (
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
)

const (
	rootPath = "./"
)

var (
	filesMap     = make(map[[sha256.Size]byte][]string)
	filesMapLock sync.Mutex

	sem  = make(chan struct{}, 64)
	wait sync.WaitGroup
)

func main() {

	var done = make(chan int)
	go func() {

		fmt.Printf("walking to all files... \n")

		var err error

		err = filepath.Walk(rootPath, func(path string, info os.FileInfo, walkErr error) error {

			if info.IsDir() {
				return nil
			}

			wait.Add(1)
			sem <- struct{}{}
			go processFile(path)

			return nil
		})
		if err != nil {
			panic(err)
		}

		wait.Wait()

		var dupCount = 0
		fmt.Printf("checking duplicates... \n")
		for _, v := range filesMap {
			if len(v) > 1 {
				dupCount++
				fmt.Println(dupCount)
				for _, fn := range v {
					fmt.Printf("    %s\n", fn)
				}
				fmt.Println()
			}
		}
		if dupCount == 0 {
			fmt.Printf("no duplicates found \n")
		} else {
			fmt.Printf("%d duplicates found \n", dupCount)
		}

		done <- 1
	}()

	<-done
}

func processFile(path string) {

	defer func() {
		wait.Done()
		<-sem
	}()

	var handleErr error

	//read the file
	var data []byte
	data, handleErr = ioutil.ReadFile(path)
	if handleErr != nil {
		fmt.Printf("WARN failed reading file %s. %s \n", path, handleErr.Error())
		return
	}

	if len(data) == 0 {
		fmt.Printf("WARN empty file %s \n", path)
		return
	}

	//hash file
	var hash = sha256.New()
	_, handleErr = hash.Write(data)
	if handleErr != nil {
		fmt.Printf("WARN failed to compute file %s hash %s \n", path, handleErr.Error())
		return
	}
	var fileHashBytes = hash.Sum(nil)
	var key [sha256.Size]byte
	copy(key[:], fileHashBytes[:sha256.Size])

	//store to map
	filesMapLock.Lock()
	if _, exist := filesMap[key]; !exist {
		filesMap[key] = make([]string, 0)
	}
	var items = filesMap[key]
	items = append(items, path)
	filesMap[key] = items
	filesMapLock.Unlock()
}
