package csv_test

import (
	"strings"
	"testing"

	"github.com/johnllao/tango/csvql/csv"
)

var testdata = `"Symbol","Name","LastSale","MarketCap","IPOyear","Sector","industry","Summary Quote"
"DDD","3D Systems Corporation","17.97","$2.05B","n/a","Technology","Computer Software: Prepackaged Software","https://www.nasdaq.com/symbol/ddd"
"MMM","3M Company","213.19","$125.06B","n/a","Health Care","Medical/Dental Instruments","https://www.nasdaq.com/symbol/mmm"
"WBAI","500.com Limited","10.25","$433.12M","2013","Consumer Services","Services-Misc. Amusement & Recreation","https://www.nasdaq.com/symbol/wbai"
"WUBA","58.com Inc.","66.52","$9.81B","2013","Technology","Computer Software: Programming, Data Processing","https://www.nasdaq.com/symbol/wuba"
"EGHT","8x8 Inc","20.29","$1.89B","n/a","Technology","EDP Services","https://www.nasdaq.com/symbol/eght"`

func TestCSVLoad(t *testing.T) {

	var rdr = strings.NewReader(testdata)
	var doc = csv.Load(rdr)

	var header = doc.Header()
	if ok := assertIntNotEqual(t, len(header), 8, "Invalid number of headers"); !ok {
		return
	}
	assertStringNotEqual(t, header[0], "Symbol", "Invalid header")
	assertStringNotEqual(t, header[7], "Summary Quote", "Invalid header")

	var data = doc.Data()
	if ok := assertIntNotEqual(t, len(data), 5, "Invalid number of records"); !ok {
		return
	}
	assertStringNotEqual(t, data[0][0], "DDD", "Invalid data")
	assertStringNotEqual(t, data[4][7], "https://www.nasdaq.com/symbol/eght", "Invalid data")
}

func assertIntNotEqual(t *testing.T, got int, expected int, msg string) bool {
	if got != expected {
		t.Errorf("%s. Got %d. Expected %d", msg, got, expected)
		return false
	}
	return true
}

func assertStringNotEqual(t *testing.T, got string, expected string, msg string) bool {
	if got != expected {
		t.Errorf("%s. Got '%s'. Expected '%s'", msg, got, expected)
		return false
	}
	return true
}
