package csv

import (
	"encoding/csv"
	"io"
)

// Document handles the CSV data
type Document struct {
	header []string
	data   [][]string
}

// Header returns the CSV header
func (d *Document) Header() []string {
	return d.header
}

// Data returns the CSV data
func (d *Document) Data() [][]string {
	return d.data
}

// Load reads the csv and returns the header and data slice
func Load(src io.Reader) (document *Document) {

	document = &Document{
		data: make([][]string, 0),
	}

	var rdr = csv.NewReader(src)
	var rowIx = 0

	for {
		var err error
		var rec []string

		rec, err = rdr.Read()
		if err != nil && err == io.EOF {
			break
		}
		if err != nil {
			continue
		}
		rowIx++

		if rowIx == 1 {
			document.header = rec
			continue
		}

		document.data = append(document.data, rec)
	}

	return
}
