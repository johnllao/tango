package main

import (
	"io/ioutil"
	"net/http"
)

type httputil struct {
	url string
}

func newhttputil(url string) *httputil {
	return &httputil{
		url: url,
	}
}

func (u *httputil) get() ([]byte, error) {

	var err error
	var resp *http.Response
	var httpcli = http.Client{}
	resp, err = httpcli.Get(u.url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var data []byte
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return data, nil
}
