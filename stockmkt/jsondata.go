package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
)

type stock struct {
	Ticker      string  `json:"Ticker"`
	Price       float64 `json:"Price"`
	CompanyName string  `json:"companyName"`
}

type jsondata []byte

func (d jsondata) tohash() []byte {
	var h = sha256.New()
	h.Write([]byte(d))
	return h.Sum(nil)
}

func (d jsondata) tostocks() ([]stock, error) {

	var data = []byte(d)

	var list []stock
	var jsondecoder = json.NewDecoder(bytes.NewReader(data[5 : len(data)-5]))
	var err = jsondecoder.Decode(&list)
	if err != nil {
		return nil, err
	}
	return list, nil
}
