package main

import (
	"encoding/hex"
	"log"
	"os"
)

const (
	allrealtimestocksurl = "https://financialmodelingprep.com/api/stock/real-time/all"
	allstocksurl         = "https://financialmodelingprep.com/api/stock/list/all"
	majorixurl           = "https://financialmodelingprep.com/api/majors-indexes?datatype=json"
	companyprofurl       = "https://financialmodelingprep.com/api/company/profile/{symbol}?datatype=json"
)

func main() {

	var applog = log.New(os.Stdin, "", log.LstdFlags)

	var err error
	var data []byte
	data, err = newhttputil(allstocksurl).get()
	if err != nil {
		applog.Fatal(err)
		return
	}

	applog.Print(hex.EncodeToString(jsondata(data).tohash()))
}
