package csvreader

import (
	"encoding/csv"
	"io"
)

// ToSlice returns a slice of data
func ToSlice(reader io.Reader, factory func([]string, []string) interface{}) []interface{} {
	var isHeader = true
	var header []string

	var csvReader = csv.NewReader(reader)
	var list = make([]interface{}, 0)
	for {
		var err error
		var record []string
		record, err = csvReader.Read()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				continue
			}
		}
		if isHeader {
			isHeader = false
			header = record
		} else {
			list = append(list, factory(header, record))
		}
	}
	return list
}

// ToStringMap returns a map of data
func ToStringMap(reader io.Reader, keySelector func([]string, []string) string, factory func([]string, []string) interface{}) map[string]interface{} {
	var isHeader = true
	var header []string

	var csvReader = csv.NewReader(reader)
	var m = make(map[string]interface{})
	for {
		var err error
		var record []string
		record, err = csvReader.Read()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				continue
			}
		}
		if isHeader {
			isHeader = false
			header = record
		} else {
			var key string
			key = keySelector(header, record)
			m[key] = factory(header, record)
		}
	}
	return m
}
