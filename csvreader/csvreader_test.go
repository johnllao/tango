package csvreader

import (
	"strings"
	"testing"
)

var testdata = `"Symbol","Name","LastSale","MarketCap","IPOyear","Sector","industry","Summary Quote"
"DDD","3D Systems Corporation","17.97","$2.05B","n/a","Technology","Computer Software: Prepackaged Software","https://www.nasdaq.com/symbol/ddd"
"MMM","3M Company","213.19","$125.06B","n/a","Health Care","Medical/Dental Instruments","https://www.nasdaq.com/symbol/mmm"
"WBAI","500.com Limited","10.25","$433.12M","2013","Consumer Services","Services-Misc. Amusement & Recreation","https://www.nasdaq.com/symbol/wbai"
"WUBA","58.com Inc.","66.52","$9.81B","2013","Technology","Computer Software: Programming, Data Processing","https://www.nasdaq.com/symbol/wuba"
"EGHT","8x8 Inc","20.29","$1.89B","n/a","Technology","EDP Services","https://www.nasdaq.com/symbol/eght"`

func TestToSlice(t *testing.T) {

	type company struct {
		Symbol string
		Name   string
	}

	var reader = strings.NewReader(testdata)
	var list = ToSlice(reader, func(header []string, row []string) interface{} {
		return company{Symbol: row[0], Name: row[1]}
	})

	var c company
	var ok bool

	if c, ok = list[0].(company); ok {
		if c.Symbol != "DDD" {
			t.Errorf("Invalid Symbol %s", c.Symbol)
		}
		if c.Name != "3D Systems Corporation" {
			t.Errorf("Invalid Name %s", c.Name)
		}
	} else {
		t.Errorf("Cannot cast item to company %v", list[0])
	}

	if c, ok = list[4].(company); ok {
		if c.Symbol != "EGHT" {
			t.Errorf("Invalid Symbol %s", c.Symbol)
		}
		if c.Name != "8x8 Inc" {
			t.Errorf("Invalid Name %s", c.Name)
		}
	} else {
		t.Errorf("Cannot cast item to company %v", list[0])
	}
}

func TestToStringMap(t *testing.T) {

	type company struct {
		Symbol string
		Name   string
	}

	var reader = strings.NewReader(testdata)
	var m = ToStringMap(reader,
		func(header []string, row []string) string {
			return row[0]
		},
		func(header []string, row []string) interface{} {
			return company{Symbol: row[0], Name: row[1]}
		},
	)

	var c company
	var ok bool

	if _, ok = m["DDD"]; ok {
		if c, ok = m["DDD"].(company); ok {
			if c.Name != "3D Systems Corporation" {
				t.Errorf("Invalid Name %s", c.Name)
			}
		} else {
			t.Errorf("Cannot cast item to company %v", m["DDD"])
		}
	} else {
		t.Error("Cannot find 'DDD' key from map")
	}

	if _, ok = m["EGHT"]; ok {
		if c, ok = m["EGHT"].(company); ok {
			if c.Name != "8x8 Inc" {
				t.Errorf("Invalid Name %s", c.Name)
			}
		} else {
			t.Errorf("Cannot cast item to company %v", m["EGHT"])
		}
	} else {
		t.Error("Cannot find 'EGHT' key from map")
	}

}
