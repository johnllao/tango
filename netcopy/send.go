package main

import (
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
)

func startsend(args []string) {

	var filenamepath string
	var receiveraddr string

	var flagset = flag.NewFlagSet("send", flag.ExitOnError)
	flagset.StringVar(&filenamepath, "i", "", "path and filename of the file")
	flagset.StringVar(&receiveraddr, "s", "", "receiver host and port")
	flagset.Parse(args)

	var err error

	var f = os.Stdin
	if filenamepath != "" {
		f, err = os.Open(filenamepath)
		if err != nil {
			fmt.Printf("ERR %s \n", err.Error())
			return
		}
		defer f.Close()
	}

	var conn net.Conn
	conn, err = net.Dial("tcp", receiveraddr)
	if err != nil {
		fmt.Printf("ERR %s \n", err.Error())
		return
	}
	defer conn.Close()

	if filenamepath != "" {
		conn.Write([]byte(filepath.Base(filenamepath)+"\n"))
	} 
 	io.Copy(conn, f)
}
