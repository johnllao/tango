package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"time"
)

func startreceive(args []string) {

	var port int

	var flagset = flag.NewFlagSet("receive", flag.ExitOnError)
	flagset.IntVar(&port, "p", 55501, "port number")
	flagset.Parse(args)

	var err error
	var listener net.Listener
	listener, err = net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		fmt.Printf("ERR %s \n", err.Error())
		return
	}
	defer listener.Close()

	fmt.Printf("receiver started at port %d \n", port)

	for {
		var conn net.Conn
		conn, err = listener.Accept()
		if err != nil {
			fmt.Printf("WARN %s \n", err.Error())
			continue
		}
		go download(conn)
	}
}

func download(conn net.Conn) {
	defer conn.Close()

	var started = time.Now()

	var err error

	var r = bufio.NewReader(conn)
	var b []byte
	b, err = r.ReadBytes('\n')
	if err != nil {
		fmt.Printf("WARN %s \n", err.Error())
		conn.Write([]byte(err.Error() + "\n"))
		return
	}

	var filename = string(b[:len(b)-1])

	var f *os.File
	f, err = os.Create("./" + filename)
	if err != nil {
		fmt.Printf("WARN %s \n", err.Error())
		conn.Write([]byte(err.Error() + "\n"))
		return
	}
	defer f.Close()

	for {

		var size int
		var buff = make([]byte, 1024)
		size, err = r.Read(buff)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Printf("WARN %s \n", err.Error())
			conn.Write([]byte(err.Error() + "\n"))
			return
		}
		f.Write(buff[:size])
	}

	conn.Write([]byte("complete\n"))
	fmt.Printf("successfully downloaded '%s'. elapsed %v \n", filename, time.Since(started))
}
