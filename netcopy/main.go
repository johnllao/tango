package main

import (
	"flag"
	"fmt"
	"os"
)

var (
	flagset = flag.NewFlagSet("main", flag.ExitOnError)
)

func main() {

	if len(os.Args) < 2 {
		fmt.Printf("Missing command \n")
		return
	}

	var cmd = os.Args[1]
	var cmdargs = os.Args[2:]

	if cmd == "receive" {
		startreceive(cmdargs)
		return
	}

	if cmd == "send" {
		startsend(cmdargs)
		return
	}

	fmt.Printf("Invalid command '%s' \n", cmd)
}
