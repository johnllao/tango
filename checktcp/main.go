package main

import (
	"fmt"
	"net"
	"os"
	"time"
)

const (
	// NETWORK default network type (e.g. tcp)
	NETWORK = "tcp"
)

func main() {

	var err error
	var conn net.Conn
	var addr string
	for _, addr = range os.Args[1:] {
		conn, err = net.DialTimeout(NETWORK, addr, 5*time.Second)
		if err != nil {
			fmt.Printf("%s - FAILED \n", addr)
			continue
		}
		conn.Close()
		fmt.Printf("%s - SUCCEEDED \n", addr)
	}

}
